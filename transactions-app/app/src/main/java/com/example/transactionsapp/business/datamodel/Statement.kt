package com.example.transactionsapp.business.datamodel

import com.google.gson.annotations.Expose

data class Statement(
    @Expose val from: String,
    @Expose val to: String,
    @Expose val dateTime: String,
    @Expose val credit: String,
    @Expose val debit: String,
    @Expose val details: String,
    @Expose val status: String,
    @Expose val transactionID: Int,
    @Expose val currencyCode: String
)