package com.example.transactionsapp.config

import android.app.Application
import com.example.transactionsapp.config.koin.creatorModule
import com.example.transactionsapp.config.koin.networkModule
import com.example.transactionsapp.config.koin.viewModelModule
import org.koin.android.ext.android.startKoin

class TransactionsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(networkModule, viewModelModule, creatorModule))
    }
}