package com.example.transactionsapp.ui.transactiondetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.databinding.TransactionDetailsFragmentLayoutBinding
import com.example.transactionsapp.ui.transactionslist.TransactionsListFragment

class TransactionDetailsFragment : Fragment() {

    private lateinit var binding: TransactionDetailsFragmentLayoutBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.transaction_details_fragment_layout, container, false)

        val transaction = arguments!!.getParcelable<Transaction>(TransactionsListFragment.BUNDLE_KEY_ID)
        setTransactionDetails(transaction!!)

        return binding.root
    }

    private fun setTransactionDetails(transaction: Transaction) {
        binding.transactionDetailsItem.setTransactionDetailsItem(transaction)
    }

    companion object {
        const val TAG_TRANSACTION_DETAILS_FRAGMENT = "transactionDetailsFragmentTag"

        fun newInstance(bundle: Bundle?): TransactionDetailsFragment {
            val transactionDetailsFragment = TransactionDetailsFragment()
            if (bundle != null) {
                transactionDetailsFragment.arguments = bundle
            }
            return transactionDetailsFragment
        }
    }
}