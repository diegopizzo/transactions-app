package com.example.transactionsapp.business.datamodel

import com.google.gson.annotations.Expose

data class Response(@Expose val result: Result)