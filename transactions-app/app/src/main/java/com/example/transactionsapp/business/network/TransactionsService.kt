package com.example.transactionsapp.business.network

import com.example.transactionsapp.business.datamodel.Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface TransactionsService {

    companion object {
        const val ENDPOINT = "https://private-55d5f0-redcloud.apiary-mock.com"
    }

    @Headers("Content-Type: application/json")
    @GET("/transactions")
    fun getTransactions(): Single<Response>
}