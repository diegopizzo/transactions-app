package com.example.transactionsapp.ui.transactionslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.databinding.TransactionsListFragmentLayoutBinding
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

class TransactionsListFragment : Fragment() {

    private lateinit var binding: TransactionsListFragmentLayoutBinding
    private val viewModel: TransactionsListViewModel by viewModel()
    private lateinit var adapter: TransactionsListAdapter
    private lateinit var onFragmentInteractionListener: OnFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.transactions_list_fragment_layout, container, false)
        binding.viewModel = viewModel
        setRecyclerView()
        viewModel.getAllTransactions()

        viewModel.transactionsLiveData.observe(
            this,
            Observer { transactions -> adapter.addTransactionsOnList(transactions) })

        viewModel.errorMessageLiveData.observe(this, Observer { message -> showMessage(message) })

        viewModel.progressBarLiveData.observe(
            this,
            Observer { value -> binding.progressBarTransactions.visibility = value })

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onFragmentInteractionListener = activity as OnFragmentInteractionListener
    }

    private fun setRecyclerView() {
        binding.transactionsRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter = TransactionsListAdapter(object : TransactionsListAdapter.OnTransactionClickListener {
            override fun onTransactionClick(transaction: Transaction) {
                onFragmentInteractionListener.onFragmentInteraction(transaction)
            }
        })
        binding.transactionsRecyclerView.adapter = adapter
    }

    private fun showMessage(message: String?) {
        if (message != null) {
            Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
        }
    }

    companion object {
        const val TAG_TRANSACTIONS_LIST_FRAGMENT = "transactionsListFragmentTag"
        const val BUNDLE_KEY_ID = "transactionKeyId"

        fun newInstance(bundle: Bundle?): TransactionsListFragment {
            val transactionsListFragment = TransactionsListFragment()
            if (bundle != null) {
                transactionsListFragment.arguments = bundle
            }
            return transactionsListFragment
        }
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(transaction: Transaction)
    }
}