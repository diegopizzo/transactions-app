package com.example.transactionsapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.ui.transactiondetails.TransactionDetailsFragment
import com.example.transactionsapp.ui.transactionslist.TransactionsListFragment

class MainActivity : AppCompatActivity(), TransactionsListFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startTransactionsListFragment()
    }

    override fun onFragmentInteraction(transaction: Transaction) {
        val bundle = Bundle()
        bundle.putParcelable(TransactionsListFragment.BUNDLE_KEY_ID, transaction)
        startTransactionDetailsFragment(bundle)
    }

    private fun startTransactionsListFragment() {
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(TransactionsListFragment.TAG_TRANSACTIONS_LIST_FRAGMENT)
        if (fragment == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                TransactionsListFragment.newInstance(null),
                TransactionsListFragment.TAG_TRANSACTIONS_LIST_FRAGMENT
            ).commit()
        }
    }

    private fun startTransactionDetailsFragment(bundle: Bundle) {
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(TransactionDetailsFragment.TAG_TRANSACTION_DETAILS_FRAGMENT)
        if (fragment == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                TransactionDetailsFragment.newInstance(bundle),
                TransactionDetailsFragment.TAG_TRANSACTION_DETAILS_FRAGMENT
            ).addToBackStack(TransactionDetailsFragment.TAG_TRANSACTION_DETAILS_FRAGMENT).commit()
        }
    }
}
