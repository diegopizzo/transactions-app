package com.example.transactionsapp.config.koin

import com.example.transactionsapp.ui.transactionslist.TransactionsListViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {
    viewModel { TransactionsListViewModel(get()) }
}