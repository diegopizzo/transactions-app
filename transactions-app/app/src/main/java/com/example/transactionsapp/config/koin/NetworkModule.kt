package com.example.transactionsapp.config.koin

import com.example.transactionsapp.business.creator.TransactionCreator
import com.example.transactionsapp.business.interactor.TransactionsInteractor
import com.example.transactionsapp.business.network.TransactionsService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { providesRetrofit(get()) }
    single { providesGsonConverterFactory(get()) }
    single { providesGson() }
    single { providesTransactionsService(get()) }
    single { providesTransactionsInteractor(get(), get()) }

}

fun providesRetrofit(gsonConverterFactory: GsonConverterFactory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(TransactionsService.ENDPOINT)
        .addConverterFactory(gsonConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun providesGsonConverterFactory(gson: Gson): GsonConverterFactory {
    return GsonConverterFactory.create(gson)
}

fun providesGson(): Gson {
    return GsonBuilder().serializeNulls().create()
}

fun providesTransactionsService(retrofit: Retrofit): TransactionsService {
    return retrofit.create(TransactionsService::class.java)
}

fun providesTransactionsInteractor(
    transactionsService: TransactionsService, transactionCreator: TransactionCreator
): TransactionsInteractor {
    return TransactionsInteractor(transactionsService, transactionCreator)
}