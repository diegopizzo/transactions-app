package com.example.transactionsapp.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import kotlinx.android.synthetic.main.transaction_details_custom_card_view_layout.view.*

class TransactionDetailsCustomCardView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        View.inflate(context, R.layout.transaction_details_custom_card_view_layout, this)
    }

    fun setTransactionDetailsItem(transaction: Transaction) {
        fromDetailsValueTextView.text = transaction.from
        toDetailsValueTextView.text = transaction.to
        dateTimeDetailsValueTextView.text = transaction.dateTime
        statusDetailsValueTextView.text = transaction.status
        creditDetailsValueTextView.text =
            resources.getString(R.string.currency, transaction.credit, transaction.currencyCode)
        debitDetailsValueTextView.text =
            resources.getString(R.string.currency, transaction.debit, transaction.currencyCode)
        infoDetailsValueTextView.text = transaction.details
        idDetailsValueTextView.text = transaction.transactionID
    }
}