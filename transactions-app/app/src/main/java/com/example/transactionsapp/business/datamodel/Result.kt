package com.example.transactionsapp.business.datamodel

import com.google.gson.annotations.Expose

data class Result(@Expose val statements: List<Statement>)