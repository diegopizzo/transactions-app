package com.example.transactionsapp.ui.transactionslist

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.business.interactor.TransactionsInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class TransactionsListViewModel(private val transactionsInteractor: TransactionsInteractor) : ViewModel() {

    private lateinit var disposable: Disposable

    private val transactionsMutableLiveData: MutableLiveData<List<Transaction>> = MutableLiveData()
    val transactionsLiveData: LiveData<List<Transaction>> get() = transactionsMutableLiveData

    private val progressBarMutableLiveData: MutableLiveData<Int> = MutableLiveData()
    val progressBarLiveData: LiveData<Int> get() = progressBarMutableLiveData

    private val errorMessageMutableLiveData: MutableLiveData<String> = MutableLiveData()
    val errorMessageLiveData: LiveData<String> get() = errorMessageMutableLiveData


    fun getAllTransactions() {
        val single = transactionsInteractor.getTransactionsFromService()
        disposable = single.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doFinally { onEnd() }
            .subscribe({ statements -> onSuccess(statements) }, { onError() })
    }

    private fun onStart() {
        progressBarMutableLiveData.value = View.VISIBLE
        errorMessageMutableLiveData.value = null
    }

    private fun onEnd() {
        progressBarMutableLiveData.value = View.GONE
    }

    private fun onSuccess(transactions: List<Transaction>) {
        transactionsMutableLiveData.value = transactions
    }

    private fun onError() {
        errorMessageMutableLiveData.value = ERROR_MESSAGE
    }

    override fun onCleared() {
        super.onCleared()
        if (!disposable.isDisposed) disposable.dispose()
    }

    companion object {
        private const val ERROR_MESSAGE = "Error: it was not possible retrieve the list"
    }
}