package com.example.transactionsapp.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import kotlinx.android.synthetic.main.transaction_custom_card_view_layout.view.*

class TransactionCustomCardView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        View.inflate(context, R.layout.transaction_custom_card_view_layout, this)
    }

    fun setTransactionItem(transaction: Transaction) {
        fromValueTextView.text = transaction.from
        toValueTextView.text = transaction.to
        dateTimeValueTextView.text = transaction.dateTime
        statusValueTextView.text = transaction.status
        outgoingsValueTextView.text = transaction.outgoingsType
    }
}