package com.example.transactionsapp.ui.transactionslist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.transactionsapp.R
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.ui.customview.TransactionCustomCardView
import kotlinx.android.synthetic.main.transactions_list_item_layout.view.*

class TransactionsListAdapter(private val listener: OnTransactionClickListener) :
    RecyclerView.Adapter<TransactionsListAdapter.ViewHolder>() {

    private val transactionList: MutableList<Transaction> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.transactions_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    fun addTransactionsOnList(items: List<Transaction>) {
        transactionList.clear()
        transactionList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val transaction = transactionList[position]
        holder.transactionItem.setTransactionItem(transaction)
        holder.setOnClickListener(transaction)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val transactionItem: TransactionCustomCardView = itemView.transactionItem

        fun setOnClickListener(transaction: Transaction) {
            itemView.setOnClickListener {
                listener.onTransactionClick(transaction)
            }
        }
    }

    interface OnTransactionClickListener {
        fun onTransactionClick(transaction: Transaction)
    }
}