package com.example.transactionsapp.business.dataviewmodel

import android.os.Parcel
import android.os.Parcelable

class Transaction(
    val from: String,
    val to: String,
    val dateTime: String,
    val credit: String,
    val debit: String,
    val outgoingsType: String,
    val details: String,
    val status: String,
    val transactionID: String,
    val currencyCode: String

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(from)
        parcel.writeString(to)
        parcel.writeString(dateTime)
        parcel.writeString(credit)
        parcel.writeString(debit)
        parcel.writeString(outgoingsType)
        parcel.writeString(details)
        parcel.writeString(status)
        parcel.writeString(transactionID)
        parcel.writeString(currencyCode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Transaction> {
        override fun createFromParcel(parcel: Parcel): Transaction {
            return Transaction(parcel)
        }

        override fun newArray(size: Int): Array<Transaction?> {
            return arrayOfNulls(size)
        }
    }
}