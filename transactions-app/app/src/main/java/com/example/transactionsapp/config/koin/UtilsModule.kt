package com.example.transactionsapp.config.koin

import com.example.transactionsapp.business.creator.TransactionCreator
import org.koin.dsl.module.module

val creatorModule = module {
    single { TransactionCreator() }
}