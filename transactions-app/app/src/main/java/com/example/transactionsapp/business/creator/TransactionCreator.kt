package com.example.transactionsapp.business.creator

import com.example.transactionsapp.business.datamodel.Statement
import com.example.transactionsapp.business.dataviewmodel.Transaction
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class TransactionCreator {

    fun createTransactionViewModel(statement: Statement): Transaction {
        return Transaction(
            statement.from,
            statement.to,
            formatDateTimeString(statement.dateTime),
            statement.credit,
            statement.debit,
            checkIfCreditOrDebitOutgoings(statement.credit),
            statement.details,
            statement.status,
            statement.transactionID.toString(),
            statement.currencyCode
        )
    }

    fun createTransactionViewModelList(statements: List<Statement>): List<Transaction> {
        return statements.map { statement -> createTransactionViewModel(statement) }
    }

    private fun checkIfCreditOrDebitOutgoings(credit: String): String {
        val creditValue = NumberFormat.getNumberInstance().parse(credit)
        return if (creditValue != null && creditValue.toInt() != 0) {
            Outgoings.CREDIT.outgoings
        } else {
            Outgoings.DEBIT.outgoings
        }
    }

    private fun formatDateTimeString(dateTime: String): String {
        val parser = SimpleDateFormat(DATE_INPUT_PATTERN, Locale.ROOT)
        val formatter = SimpleDateFormat(DATE_OUTPUT_PATTERN, Locale.ROOT)
        return formatter.format(parser.parse(dateTime))
    }
}

enum class Outgoings(val outgoings: String) {
    CREDIT("Credit"), DEBIT("Debit")
}

private const val DATE_INPUT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
private const val DATE_OUTPUT_PATTERN = "dd-MM-yyyy HH:mm"