package com.example.transactionsapp.business.interactor

import com.example.transactionsapp.business.creator.TransactionCreator
import com.example.transactionsapp.business.datamodel.Response
import com.example.transactionsapp.business.dataviewmodel.Transaction
import com.example.transactionsapp.business.network.TransactionsService
import io.reactivex.Single

class TransactionsInteractor(
    private val transactionsService: TransactionsService,
    private val transactionCreator: TransactionCreator
) {

    fun getTransactionsFromService(): Single<List<Transaction>> {
        return transactionsService.getTransactions()
            .map { response: Response -> transactionCreator.createTransactionViewModelList(response.result.statements) }
    }
}