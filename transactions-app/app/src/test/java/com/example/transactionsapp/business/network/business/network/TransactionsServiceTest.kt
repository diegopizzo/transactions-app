package com.example.transactionsapp.business.network.business.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.transactionsapp.business.network.TransactionsService
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class TransactionsServiceTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var transactionsService: TransactionsService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        transactionsService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(TransactionsService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun whenCallApiThenCheckIfThePathOfRequestIsCorrect() {
        enqueueResponse("transaction_list.json")
        transactionsService.getTransactions().test().assertComplete()
        assertThat(mockWebServer.takeRequest().path, CoreMatchers.`is`("/transactions"))
    }

    @Test
    fun whenCallApiThenCheckIfTheIdOfFirstTransactionIsCorrect() {
        enqueueResponse("transaction_list.json")
        val response = transactionsService.getTransactions().test().assertComplete()
        response.assertValue { responseValue -> responseValue.result.statements[0].transactionID == 500199 }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(source.readString(Charsets.UTF_8)))
    }
}