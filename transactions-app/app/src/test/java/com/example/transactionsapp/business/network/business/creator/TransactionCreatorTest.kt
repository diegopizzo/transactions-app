package com.example.transactionsapp.business.network.business.creator

import com.example.transactionsapp.business.creator.Outgoings
import com.example.transactionsapp.business.creator.TransactionCreator
import com.example.transactionsapp.business.datamodel.Statement
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TransactionCreatorTest {

    private val transactionCreator: TransactionCreator = TransactionCreator()

    private fun createStatement(): Statement {
        return Statement(
            "User1",
            "User2",
            "2019-02-22T18:02:01.0306612Z",
            "0,00",
            "20,00",
            "Details",
            "Success",
            1234,
            "$"
        )
    }

    @Test
    fun whenConvertStatementToTransactionDataViewModelThenCheckIsIsCorrect() {
        val statement = createStatement()
        val transaction = transactionCreator.createTransactionViewModel(statement)
        assertEquals(transaction.outgoingsType, Outgoings.DEBIT.outgoings)
        assertEquals(transaction.dateTime, "22-02-2019 18:02")
    }
}