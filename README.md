# TransactionsApp - Diego Pizzo
This is an Android app that shows a list of transactions with differents informations and the details of each.
The project was created using Android Studio 3.3.2.

**User Interface:**
This app has two views:
 - The first view shows the list of transactions through a card view where there are sender, receiver, outgoings type and date time.
 - The second view is called when the user click on the transaction and show the transaction details like the state, description and the amount.

In both views I have chosen to build a custom view so it will be possible to reuse it for others scenarios.

**Architecture:**
This project is based on MVVM pattern and written with Kotlin.

**Possible improvements:**
In the first view it could be useful to add a filter to view only debit transactions, only credit transactions or all transactions.
When the user clicks on a transaction, the clicked transaction details are shown. To achieve this the entire transaction object, was passed into the bundle but should be better to just put the transaction id in the bundle and then retrieve the transaction's details via a getTransactionById(id : String) API endpoint.

# Third-party libraries
**Koin**  
Koin is a framework for the dependency injection. I have chosen to use this framework because I think is more simple than Dagger2. Avoid writing many classes, reducing boilerplate and it's easier to understand.
Link: https://github.com/InsertKoinIO/koin

**Retrofit**  
HTTP client used the make API Requests and retrieve the data from network.
Link: https://square.github.io/retrofit/

**RxJava**  
It is useful to do async operations and react to events.
Link: https://github.com/ReactiveX/RxJava

**MockWebServer**
Library useful to make it easy to test the API calls and verify that the expected results from requests.
Link: https://github.com/square/okhttp/tree/master/mockwebserver